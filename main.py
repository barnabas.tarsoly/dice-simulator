from flask import Flask, render_template, redirect, request
from dice import *
import json


app = Flask(__name__)


@app.route('/',methods=["POST","GET"])
def index():
    if request.method == "POST":

        modif = request.form #get the modifers from the form
        try:

            modif_sides = int(modif["side"])
            modif_modifier = int(modif["modifier"])
            modif_dices = int(modif["dices"])

            dice = json.loads(throw_dice(modif_sides, modif_dices, modif_modifier)) #get throw's data, with the adjusted parameters
        except: #if something is not an integer, set a default throw
            modif_sides = 6
            modif_modifier = 0
            modif_dices = 1
            dice = json.loads(throw_dice(modif_sides, modif_dices, modif_modifier))


        throw_list = [dice[x] for x in dice.keys() if x != "modifier" and x != "result"] # make a list to show each throw one by one
       
        return render_template('index.html',result_data=dice, modifiers=[modif_sides, modif_modifier, modif_dices], throws= throw_list)
    else: #first load in the page with default parameters
        dice = json.loads(throw_dice())
        throw_list = [dice[x] for x in dice.keys() if x != "modifier" and x != "result"]
        return render_template('index.html',result_data=dice,modifiers=[6, 0, 1], throws= throw_list)

#api for the 
@app.route('/api')
def throw_dice(sides=6, dices=1 ,modifier=0):
    
    dice = dice_roll(sides, dices, modifier) #call the dice function 
    return str(json.dumps(dice))



app.run(debug=True)

