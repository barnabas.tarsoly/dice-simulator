from random import randint

def dice_roll(sides=6, dices=1,modifier=0):
    
    final = {str(x+1):randint(1, sides) for x in range(dices)} #create a dict, with each throw
    final["modifier"] = modifier #set the modifier to the dict
    result = sum([final[x] for x in final.keys()]) # count the result
    final["result"] =  result #set the result to the final dict
    
    '''
        #Optoinal: minimum result is 0

    if result <= 0:
        final["result"] = 0
    '''

    
    return final

